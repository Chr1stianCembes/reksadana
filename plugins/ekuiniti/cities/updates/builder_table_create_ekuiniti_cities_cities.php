<?php namespace Ekuiniti\Cities\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEkuinitiCitiesCities extends Migration
{
    public function up()
    {
        Schema::create('ekuiniti_cities_cities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ekuiniti_cities_cities');
    }
}
