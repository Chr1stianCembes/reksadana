<?php namespace Ekuiniti\Cities\Models;

use Model;

/**
 * Model
 */
class City extends Model
{
    // use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $connection = 'mysql_account1';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'cities';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $fillable = ['id'];

    public $hasOne = [
        'user' => ['ekuiniti\Users\Models\User','key' => 'city_id','otherKey' => 'id'],
    ];

    public $belongsTo = [
        'province' => ['ekuiniti\Provinces\Models\Province','key' => 'province_id', 'otherKey' => 'id'],
        'userktp' => ['ekuiniti\users\Models\userktp','key' => 'userktp_id', 'otherKey' => 'id' ]
    ];
}
