<?php namespace Ekuiniti\Banks\Models;

use Model;

/**
 * Model
 */
class Bank extends Model
{
    // use \October\Rain\Database\Traits\SoftDelete;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $connection = 'mysql_account1';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'banks';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'logo' => 'required'
    ];

    protected $fillable = ['id','logo'];

    /**
     * @var array Relations
     */

    public $hasOne = [
        'user' => ['ekuiniti\users\Models\User','key' => 'bank_id','otherKey' => 'id']
    ];

    public $attachOne =[
        'logo' => 'System\Models\file'
    ];

    
}
