<?php namespace Ekuiniti\Banks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEkuinitiBanksBanks extends Migration
{
    public function up()
    {
        Schema::create('ekuiniti_banks_banks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ekuiniti_banks_banks');
    }
}
