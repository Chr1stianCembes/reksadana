<?php namespace Ekuiniti\Banks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEkuinitiBanksBanks2 extends Migration
{
    public function up()
    {
        Schema::table('ekuiniti_banks_banks', function($table)
        {
            $table->dropColumn('logo');
        });
    }
    
    public function down()
    {
        Schema::table('ekuiniti_banks_banks', function($table)
        {
            $table->text('logo');
        });
    }
}
