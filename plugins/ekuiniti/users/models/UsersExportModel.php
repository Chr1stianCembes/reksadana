<?php namespace Ekuiniti\Users\Models;

use Model;

use Backend\Models\ExportModel;
use Ekuiniti\users\Models\User;

/**
 * Model
 */
class UsersExportModel extends ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $user = User::orderBy('users.id', 'ASC')
                        ->get();

        $user->each(function($user) use ($columns) {
            $user->addVisible($columns);
        });
        return $user->toArray();
    }
    // use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;



}
