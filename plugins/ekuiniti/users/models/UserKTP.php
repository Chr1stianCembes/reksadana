<?php namespace Ekuiniti\Users\Models;

use Model;

use ekuiniti\cities\Models\City;
/**
 * Model
 */
class UserKTP extends Model
{
    // use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * Set Database Connections
     */
    protected $connection = "mysql_account1";

    /**
     * @var string The database table used by the model.
     */
    public $table = 'userktp';

    protected $fillable = ['id'];

    /**
     * @var array Relations
     */
    public $hasOne = [
        
    ];

    public $belongsTo = [
        'city' => ['ekuiniti\cities\Models\City', 'key' => 'city_id','otherKey' => 'id'],
        'user' => ['Ekuiniti\users\Models\User','key' => 'id','otherKey' => 'id']
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'ktp_address' => 'required',
        'ktp_postal_code' => 'required'
    ];
}
