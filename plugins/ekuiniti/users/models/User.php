<?php namespace Ekuiniti\Users\Models;

use Model;

use ekuiniti\provinces\Models\Province;
use ekuiniti\cities\Models\city;
use ekuiniti\banks\Models\Bank;
use ekuiniti\users\Models\UserKTP;

/**
 * Model
 */
class User extends Model
{
    // use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $connection = 'mysql_account1';


    /**
     * @var string The database table used by the model.
     */
    public $table = 'users';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $guarded = ['*'];


    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        
    ];

    public $hasMany = [
        'userktp' => ['Ekuiniti\Users\Models\UserKTP' ,'key' => 'user_id', 'otherKey' => 'id'],
        'userdomicile' => ['Ekuiniti\Users\Models\UserDomicile','key' => 'user_id','otherKey'=> 'id'],
        'usercorrespondence' => ['Ekuiniti\Users\Models\UserCorrespondence','key' => 'user_id','otherKey'=> 'id']
    ];

    public $belongsTo = [
        'province' => ['Ekuiniti\provinces\Models\Province','key' => 'province_id','otherKey' => 'id'],
        'city' => ['Ekuiniti\cities\Models\City','key' => 'city_id','otherKey' => 'id'],
        'bank' => ['Ekuiniti\banks\Models\Bank','key' => 'bank_id','otherKey' => 'id']
    ];

    public function getProvinceOptions()
    {
        $provinces = Province::lists('name','id');

        return $provinces;
    }

    public function getBankOptions()
    {
        $banks = Bank::lists('name','code');

        return $banks;
    }

    public function getUserKTPOptions()
    {
        $userktp = UserKTP::lists('ktp_address','id','user_id','ktp_postal_code','ktp_country','city_id');

        return $userktp;
    }

    public function getUserDomicileOptions()
    {
        $userdomicile = UserDomicile::list('domicile_address','domicile_country','id','user_id','domicile_postal_code','city_id');

        return $userdomicile;
    }

    public function getUserCorrespondenceOptions()
    {
        $usercorrespondence = UserCorrespondence::lists('correspondence_address','correspondence_country','id','user_id','correspondence_postal_code','city_id');

        return $usercorrespondence;
    }

    public function getCityOptions()
    {
        if (isset($this->province->id))
        {
            $cities = City::where('province_id','=',$this->province->id)->lists('name','id');
        }
        else
        {
            $cities = City::lists('name','id');
        }

        return $cities;
    }

    
    public function getVerifiedHelperAttribute()
    {
        if ($this->verified == 1)
            return 'Belum Verifikasi';
        else if ($this->verified == 2)
            return 'Verifikasi';
    }
}
