<?php namespace Ekuiniti\Users\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class User extends Controller
{
    public $implement = [        
        'Backend\Behaviors\ListController',        
        'Backend\Behaviors\FormController',        
        'Backend\Behaviors\RelationController',
        'Backend\Behaviors\ImportExportController'    
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $importExportConfig = 'config_import_export.yaml';
    

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ekuiniti.Users', 'Users','user');
    }
}
