<?php namespace Ekuiniti\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEkuinitiUsersKtp extends Migration
{
    public function up()
    {
        Schema::rename('ekuiniti_users_ktp_user', 'ekuiniti_users_ktp');
    }
    
    public function down()
    {
        Schema::rename('ekuiniti_users_ktp', 'ekuiniti_users_ktp_user');
    }
}
