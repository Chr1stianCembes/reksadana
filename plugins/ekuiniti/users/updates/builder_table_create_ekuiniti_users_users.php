<?php namespace Ekuiniti\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Blueprint;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEkuinitiUsersUsers extends Migration
{
    public function up()
    {
        Schema::create('ekuiniti_users_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ekuiniti_users_users');
    }
}
