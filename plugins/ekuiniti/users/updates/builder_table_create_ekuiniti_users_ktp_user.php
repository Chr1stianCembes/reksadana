<?php namespace Ekuiniti\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEkuinitiUsersKtpUser extends Migration
{
    public function up()
    {
        Schema::create('ekuiniti_users_ktp_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ekuiniti_users_ktp_user');
    }
}
