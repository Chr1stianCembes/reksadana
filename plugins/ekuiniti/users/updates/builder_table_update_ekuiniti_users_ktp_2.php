<?php namespace Ekuiniti\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEkuinitiUsersKtp2 extends Migration
{
    public function up()
    {
        Schema::table('ekuiniti_users_ktp', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('ekuiniti_users_ktp', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
