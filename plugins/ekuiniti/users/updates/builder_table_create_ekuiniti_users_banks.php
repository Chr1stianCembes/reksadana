<?php namespace Ekuiniti\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEkuinitiUsersBanks extends Migration
{
    public function up()
    {
        Schema::create('ekuiniti_users_banks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('user_id')->unsigned();
            $table->integer('bank_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ekuiniti_users_banks');
    }
}
