<?php namespace Ekuiniti\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEkuinitiUsersUsers extends Migration
{
    public function up()
    {
        Schema::table('ekuiniti_users_users', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('ekuiniti_users_users', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
