<?php namespace Ekuiniti\Users\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEkuinitiUsersCorrespondence extends Migration
{
    public function up()
    {
        Schema::table('ekuiniti_users_correspondence', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('ekuiniti_users_correspondence', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
