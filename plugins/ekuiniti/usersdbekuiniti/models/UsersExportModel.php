<?php namespace Ekuini\Users\Models;
use Model;

use Backend\Models\ExportModel;
use Ekuiniti\Users\Models\User;

class UsersExportModel extends ExportModel
{
    public function exportData($columns, $sessionKey = null )
    {
        $user = User::orderBy('users.name', 'ASC')
                ->get();
        $user->each(function($user) use ($columns) {
            $user->addVisible($columns);
        });
        return $user->toArray();
    }
}