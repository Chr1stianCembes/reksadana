<?php namespace Ekuiniti\Usersdbekuiniti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEkuinitiUsersdbekuiniti extends Migration
{
    public function up()
    {
        Schema::create('ekuiniti_usersdbekuiniti_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ekuiniti_usersdbekuiniti_');
    }
}
