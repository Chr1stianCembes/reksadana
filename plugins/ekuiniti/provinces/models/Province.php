<?php namespace Ekuiniti\Provinces\Models;

use Model;

/**
 * Model
 */
class Province extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $connection = 'mysql_account1';
    /**
     * @var string The database table used by the model.
     */
    public $table = 'provinces';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    
    public $hasOne = [
        'user' => ['ekuiniti\users\Models\user','key' => 'province_id','otherKey' => 'id' ]
    ];

    public $hasMany = [
        'cities' => ['ekuiniti\cities\Models\city','key' => 'province_id','otherKey' => 'id']
    ];


}
