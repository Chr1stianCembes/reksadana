<?php namespace Ekuiniti\Provinces\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEkuinitiProvincesProvinces extends Migration
{
    public function up()
    {
        Schema::create('ekuiniti_provinces_provinces', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ekuiniti_provinces_provinces');
    }
}
